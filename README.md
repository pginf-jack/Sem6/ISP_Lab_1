# ISP_Lab_1 - Parity generator

A circuit for generating a parity bit for 8-bit input word. The 8-bit input word should be set by means of switches **SW0-SW7** on the prototype board. Parity is presented on the LED display: **E** - even number of ‘ones’ in the 8-bit input word, **O** - odd number of ones.

## How to simulate?

The circuit can be simulated using GHDL or Vivado:

### Simulation using GHDL

Pre-requirements:
- `ghdl` package for compilation
- `gtkwave` or a different waveform viewer for viewing waves

The project can be compiled with:
```
make compile
```

In order to run the testbench, use the `run` recipe:
```
make run
```

Waves generated during the testbench's run can be viewed using a waveform viewer such as `gtkwave`:
```
gtkwave simulation/top_tb.vcd
```

### Simulation using Vivado

Tested with: Vivado 2018.3 and 2022.2

The project requires Nexys A7 board files that are part of [Digilent's Vivado Board Files](https://github.com/Digilent/vivado-boards).

A Vivado project can be bootstrapped using the `build.tcl` script in the root of the repository:

1. Start Vivado.
1. Open Tcl Console (Window->Tcl Console)
1. Run commands:

   ```
   cd /path/to/repository
   source build.tcl
   ```

1. The project should generate shortly after which you can start the simulation
   through "Simulation->Run Simulation->Run Behavioral Simulation" option on the left.
