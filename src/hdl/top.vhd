library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_misc.all;

entity top is
port (
    sw_i : in STD_LOGIC_VECTOR (7 downto 0);
    led7_an_o : out STD_LOGIC_VECTOR (3 downto 0);
    led7_seg_o : out STD_LOGIC_VECTOR (7 downto 0)
);
end top;

architecture data_flow of top is
begin
    led7_an_o <= "1110";
    led7_seg_o <=
        "00000011"
        when xor_reduce(sw_i) = '1'
        else "01100001";
end architecture data_flow;
